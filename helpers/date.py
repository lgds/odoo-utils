# -*- coding: utf-8 -*-
"""
Description: This module provides high level date function
Author: LGDS Info team
"""


from datetime import datetime

DATETIME_FORMAT = "%Y-%m-%d %X"


def get_now() -> str:
    """ return current date with seconds"""
    return datetime.today().strftime(DATETIME_FORMAT)


def date_to_str(date: datetime, date_format=DATETIME_FORMAT) -> str:
    """ return current date with seconds"""
    return date.strftime(date_format)
