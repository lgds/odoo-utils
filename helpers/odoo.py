# -*- coding: utf-8 -*-
"""
Description: This module allow communication with odoo "API"
Author: LGDS Info team
"""

import xmlrpc.client
from typing import Tuple, Any, Optional
from commons import get_logger
from helpers.config import CONFIG

LOGGER = get_logger("helper-odoo")


class OdooAPI:
    """Class to handle  Odoo API requests."""

    url = CONFIG["odoo_info"]["url"]
    user = CONFIG["odoo_info"]["user"]
    passwd = CONFIG["odoo_info"]["passwd"]
    db = CONFIG["odoo_info"]["db"]
    common: Optional[xmlrpc.client.ServerProxy] = None
    uid: Any = None
    models: Any = None

    def __init__(self):
        """Initialize xmlrpc connection."""
        try:
            common_proxy_url = f"{self.url}/xmlrpc/2/common"
            object_proxy_url = f"{self.url}/xmlrpc/2/object"
            self.common = xmlrpc.client.ServerProxy(common_proxy_url)
            self.uid = self.common.authenticate(self.db, self.user, self.passwd, {})
            self.models = xmlrpc.client.ServerProxy(object_proxy_url)

        except Exception as odoo_connection_exception:
            LOGGER.error(
                f"Impossible to connect to odoo API due to error: \n {odoo_connection_exception}"
            )

    def get_entity_fields(self, entity: str) -> list:
        """ Create an entity fields """
        fields = self.models.execute_kw(
            self.db,
            self.uid,
            self.passwd,
            entity,
            "fields_get",
            [],
            {"attributes": ["string", "help", "type"]},
        )
        return fields

    def search_count(self, entity: str, cond: Tuple = ()) -> list:
        """Return how many lines are matching the request."""
        return self.models.execute_kw(
            self.db, self.uid, self.passwd, entity, "search_count", [cond]
        )

    def search_read(
        self,
        entity: str,
        cond: Tuple = (),
        fields: Tuple = (),
        limit: int = 3500,
        offset: int = 0,
        order: str = "id ASC",
    ) -> list:
        """Main api request, retrieving data according search conditions."""
        fields_and_context = {
            "fields": fields,
            "context": {"lang": "fr_FR", "tz": "Europe/Paris"},
            "limit": limit,
            "offset": offset,
            "order": order,
        }

        return self.models.execute_kw(
            self.db,
            self.uid,
            self.passwd,
            entity,
            "search_read",
            [cond],
            fields_and_context,
        )

    def update(self, entity: str, ids: list, fields: list):
        """Update entities which have ids, with new fields values."""
        return self.models.execute_kw(
            self.db, self.uid, self.passwd, entity, "write", [ids, fields]
        )

    def create_record(self, entity: str, fields: dict = None):
        """Create entity instance with given fields values."""
        return self.models.execute_kw(
            self.db, self.uid, self.passwd, entity, "create", [fields or {}]
        )

    def compute_taxes(self, pol_id: int = None):
        """Create entity instance with given fields values."""
        return self.models.execute_kw(
            self.db,
            self.uid,
            self.passwd,
            "purchase.order",
            "compute_taxes",
            [pol_id or {}],
        )
