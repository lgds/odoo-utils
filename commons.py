# -*- coding: utf-8 -*-
"""
Description: This module contains static variables not specific to a process
Author: LGDS Info team
"""

import logging

LOG_LEVEL = "DEBUG"


def get_logger(app_name: str = "odoo_utils") -> logging.Logger:
    """ Return a logger  """
    logging.basicConfig(format="%(asctime)s %(levelname)s:%(message)s", level=LOG_LEVEL)
    return logging.getLogger(app_name)
