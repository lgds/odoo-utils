# -*- coding: utf-8 -*-
"""
Description: This program generate purchase order with products from a CSV (for now it it designed for Vita Frais)
Author: LGDS Info team
"""

import argparse
import datetime
import json
import sys
from typing import Any, Dict, Tuple

import pandas as pd
from commons import get_logger
from helpers.config import CONFIG
from helpers.date import date_to_str, get_now
from helpers.odoo import OdooAPI

LOGGER = get_logger("generate_purchase_order")


def _get_product_supplierinfo(
    odoo: OdooAPI, product: pd.DataFrame, supplier_ids: list
) -> dict:
    """ Get a product supplier info from a product code """

    product_name = f"[{product.Code}] {product.Désignation}"

    """ Return a product supplier info valid (= where date_start <= now() <= date_end)"""
    products_supplierinfo = odoo.search_read(
        "product.supplierinfo",
        (
            ("product_code", "=", product.Code),
            ("name", "in", supplier_ids),
            ("is_product_active", "=", True),
        ),
    )

    # Filter products only between date_start and date_end
    # this seems easier in python than https://www.odoo.com/fr_FR/forum/aide-1/full-list-of-search-operators-46716
    product_supplierinfo_filtered = [
        product_supplierinfo
        for product_supplierinfo in products_supplierinfo
        if (product_supplierinfo["date_start"] or "1970-01-01")
        <= get_now()
        <= (product_supplierinfo["date_end"] or "9999-12-31")
    ]
    if len(product_supplierinfo_filtered) > 1:

        product_ids = ", ".join(
            [str(product["id"]) for product in product_supplierinfo_filtered]
        )
        LOGGER.error(
            f"Les {len(product_supplierinfo_filtered)} produits ('{product_name}') suivant ont le même "
            f"code ({product.Code}): {product_ids}. Ils ne seront pas ajoutés à la demande de prix."
        )
    elif len(product_supplierinfo_filtered) == 0 and len(products_supplierinfo) == 1:
        LOGGER.error(
            f"Le produit '{product_name}' existe mais aucun n'est actif (EAN = {product.ean}) "
            f"chez ces fournisseurs (id = {supplier_ids})"
        )
    elif len(product_supplierinfo_filtered) == 0 and len(products_supplierinfo) == 0:
        LOGGER.error(
            f"Le produit '{product_name}' n'a pas été trouvé (EAN = {product.ean}) "
            f"chez ces fournisseurs (id = {supplier_ids})"
        )
    else:
        return product_supplierinfo_filtered[0]

    return {}


def _build_purchase_order_line_payload(
    odoo: OdooAPI,
    product: pd.DataFrame,
    product_supplierinfo: dict,
    purchase_order_date: str,
) -> Tuple:
    """ build purchase order line payload """
    product_name = f"[{product.Code}] {product.Désignation}"

    product_template_id = product_supplierinfo["product_tmpl_id"][0]

    product_product_info = odoo.search_read(
        "product.product", ((("product_tmpl_id", "=", product_template_id),))
    )
    payload: Tuple[int, bool, Dict[str, Any]] = (1, False, {})
    if len(product_product_info) == 0:
        LOGGER.error(
            f"Taxe non retrouvée car le produit '{product_name}' n'a pas été trouvé"
            f" (EAN = {product.ean})"
        )
    elif len(product_product_info) > 1:
        LOGGER.error(
            f"{len(product_product_info)} produits ont été trouvés pour l'article '{product_name}' "
            f"(code = {product.Code}). Il ne sera pas ajouté à la demande de prix"
        )
    else:
        taxes_id = product_product_info[0]["supplier_taxes_id"]

        product_qty_package = product.quantite / product_supplierinfo["package_qty"]
        if product_qty_package % 1 > 0:
            LOGGER.error(
                f"Attention la quantité du produit '{product_name}' "
                f"(id = {product_template_id} / EAN = {product.ean}) semble incorrecte "
                f"car le collisage ne peut pas conditionner la quantité totale de produit"
            )

        payload = (
            0,
            False,
            {
                "product_id": product_product_info[0]["id"],
                "package_qty": product_supplierinfo["package_qty"],
                "product_qty_package": product_qty_package,
                "product_qty": product.quantite,
                "product_uom": product_supplierinfo["product_uom"][0],
                "price_unit": product_supplierinfo["base_price"],
                "name": product_name,
                "date_planned": purchase_order_date,
                "taxes_id": ((6, False, (taxes_id[0],)),) if taxes_id else "",
            },
        )

    return payload


def generate_purchase_order(
    product_ordered: pd.DataFrame, supplier_ids: list, purchase_order_date: str
):
    """ Generate a purchase order and its lines """

    odoo = OdooAPI()
    url = CONFIG["odoo_info"]["url"]

    suppliers_payloads: dict = {supplier: [] for supplier in supplier_ids}

    for product in product_ordered.itertuples():

        product_supplierinfo = _get_product_supplierinfo(odoo, product, supplier_ids)

        if product_supplierinfo:
            supplier_id = product_supplierinfo["name"][0]
            purchase_order_line_payload = _build_purchase_order_line_payload(
                odoo, product, product_supplierinfo, purchase_order_date
            )

            suppliers_payloads[supplier_id].append(purchase_order_line_payload)

    LOGGER.info(
        f"Résumé: Pour un total de {len(product_ordered.index)} produits à traiter"
    )

    for supplier_id, purchase_order_line_payloads in suppliers_payloads.items():

        log_message = (
            f"- {len(purchase_order_line_payloads)} produits ont été ajoutés "
            f"à la demande de prix du fournisseur id = {supplier_id}. "
        )
        # if there is any product processed for this supplier
        if len(purchase_order_line_payloads) > 0:
            payload_load = {
                "partner_id": supplier_id,
                "date_planned": purchase_order_date,
                "order_line": purchase_order_line_payloads,
            }

            # Create purchase order
            purchase_order_id = odoo.create_record("purchase.order", payload_load)

            log_message += f"Voir la demande de prix: {url}/web#id={purchase_order_id}&model=purchase.order"

        LOGGER.info(log_message)


if __name__ == "__main__":
    """Generate a purchase order and its lines from a filepath, supplier ids and a purchase date
    Args:
        filepath (str): file to be processed filepath
        supplier_name (str): name of the supplier
        date(str): Purchase order date planned (date format: YYYY-MM-dd) Ex: 2021-01-30
    Returns:
    """

    SUPPLIERS_IDS = json.loads(CONFIG["odoo_info"]["suppliers_ids"])
    SUPPLIERS_NAMES = list(SUPPLIERS_IDS.keys())

    PARSER = argparse.ArgumentParser()
    PARSER.add_argument(
        "--filepath",
        help="Entrer le chemin relatif au fichier csv à traiter",
    )
    PARSER.add_argument(
        "--supplier_name",
        help=f"Renseigner le nom du fournisseur ({SUPPLIERS_NAMES})",
    )
    PARSER.add_argument(
        "--date",
        help="Entrer une date (format: YYYY-MM-dd) (Ex: 2021-01-27)",
        type=datetime.date.fromisoformat,
    )
    ARGS = PARSER.parse_args()

    if ARGS.supplier_name not in SUPPLIERS_NAMES:
        LOGGER.error(
            f"Le nom de fournisseur '{ARGS.supplier_name}' est invalide. Choix: {SUPPLIERS_NAMES} "
        )
        sys.exit()

    PRODUCT_ORDERED = pd.read_csv(ARGS.filepath, sep=";")
    PRODUCT_ORDERED.rename(
        columns={"Code EAN": "ean", "Quantité": "quantite"}, inplace=True
    )

    generate_purchase_order(
        PRODUCT_ORDERED,
        SUPPLIERS_IDS[ARGS.supplier_name],
        date_to_str(ARGS.date, "%Y-%m-%d"),
    )
