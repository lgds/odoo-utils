# Odoo utils Repo

This repo aims to generate purchase order with products from a CSV (for now it it designed for Vita Frais)

If you want to participate to the code you can follow the [Developpement](#developpement) part or directly [execute the script](#run-the-export-script)

## Developpement

You can use following tools for proper python installation and package manager:
 - [PyEnv Virtualenv](https://realpython.com/intro-to-pyenv/)
 - (optional)[Poetry](https://blog.jayway.com/2019/12/28/pyenv-poetry-saviours-in-the-python-chaos/)


### Dev environment installation

**1. (optional) Install [pyenv](https://github.com/pyenv/pyenv#homebrew-on-macos) (python version manager)**

```bash
brew install pyenv # On mac os
```

**2. (optional) Install 3.9.1 (with or without pyenv)**

```bash
pyenv install 3.9.1
pyenv global 3.9.1
pyenv shell 3.9.1

# Test
python3.9 -V
# Python 3.9.1

pyenv virtualenv 3.9.1 odoo-utils

pyenv local odoo-utils # will generate a .python-version file

echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.zshrc # Enable virtualenv autoactivation to your shell 
```

**3. (optional) Install [poetry](https://github.com/python-poetry/poetry) (python package-management system)**

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

**4. (optional) Add poetry and pyenv to ~/.zshrc**

```bash
# Add poetry to your shell
export PATH="$HOME/.poetry/bin:$PATH"

# Add pyenv initializer to shell startup script.
echo -e '\nif command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi' >> ~/.zshrc

# Reload your profile.
source ~/.zshrc

# Test
poetry -V
# Poetry version 1.1.4
```

**5. Add folder path to your python path**

```bash
export PYTHONPATH=$PYTHONPATH:[Path]/odoo-utils
```

**6. Install repo's dependencies**

```bash
# Install dependencies
poetry install 
```

**7. (optional) Update requirements files**

If you added pacckage, update requirements files
Update requirements

 ```bash
poetry export -f requirements.txt --dev > requirements-dev.txt
poetry export -f requirements.txt > requirements.txt
```


## Run the export script

First you need to allow the program to have access to your [odoo](https://www.odoo.com/documentation/9.0/api_integration.html), configure the `.conf` file.

 ```bash
cp .conf.local .conf
```

The file looks like:

```
[odoo_info]
url : http://localhost:8069
user : utilisateur
passwd : mdp
db : foodcoops
suppliers_ids : {"vita":[1,4,39]}
```

Then run the script. (NB. don't forget to set your python path)

 ```bash
python3.9 generate_purchase_order.py --supplier_name=vita --date=2021-01-30 --filepath=../csv_file/filepath.csv
```